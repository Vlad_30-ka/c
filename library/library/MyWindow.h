//#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include <string>
#include <conio.h> 
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <io.h> 
#include <vector>
#include <algorithm>
#include <map>

using namespace std;



class Window
{
protected:
	int left, top, right, bottom;
	char s[6];
	int bgcolor;
	int bordercolor;
	int shadowcolor;
	int textcolor;
public:
	Window(int left_ = 0, int top_ = 0, int right_ = 0, int bottom_ = 0);
	void Move(int left_, int top_, int right_ = 0, int bottom_ = 0);
	void AlignCenter();
	void SetWidth(int w);
	void SetHeight(int h);
	void SetBorder(char *s_) { for (int i = 0; i < 6; i++) s[i] = s_[i]; }
	void SetBgColor(int bgcolor) { this->bgcolor = bgcolor; }
	void SetBorderColor(int bordercolor) { this->bordercolor = bordercolor; }
	void SetShadowColor(int shadowcolor) { this->shadowcolor = bordercolor; }
	void SetTextColor(int textcolor) { this->textcolor = textcolor; }
	//void WriteString(int x, int y, const char * str, int st = 0, int w = 35000, int h = 35000);
	int GetWidth() { return right - left - 1; }
	int GetHeight() { return bottom - top - 1; }
	int GetBgColor() { return bgcolor; }
	int GetBorderColor() { return bordercolor; }
	int GetShadowColor() { return shadowcolor; }
	int GetTextColor() { return textcolor; }
	void Show(int, int, int, int, int kod = 0);
	void fill(int t = 15, int bc = 1);
	virtual void Draw() {}
	~Window() {};
};

class Menu : public Window
{
	fstream file;

protected:
	int color_a;
	int bgcolor_a;
	int choice, first;
public:

	vector <string> items;

	Menu(int left_ = 0, int top_ = 0, int right_ = 0, int bottom_ = 0);
	unsigned int GetCount() { return items.size(); }
	void SetBgColorA(int bgcolor_a_) { bgcolor_a = bgcolor_a_; }
	int GetBgColorA() { return bgcolor_a; }
	void SetColorA(int color_a_) { color_a = color_a_; }
	int GetColorA() { return color_a; }
	void DrawMenu(int j, int y);
	virtual void DoMenu(Menu &) { cout << " "; };
	void Message(const char msg[], int kod = 0);
	const char* recode(const char *txt);
	
	~Menu() {};
};